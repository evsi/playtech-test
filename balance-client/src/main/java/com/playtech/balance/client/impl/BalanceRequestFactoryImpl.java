package com.playtech.balance.client.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.playtech.balance.BalanceRequest;
import com.playtech.balance.client.BalanceRequestFactory;
import com.playtech.util.sequence.SequenceGenerator;

@Singleton
public class BalanceRequestFactoryImpl implements BalanceRequestFactory {
    private static final String[] blackListedNames = {
        "qew",
        "qwe",
        "eqw",
        "ewq",
        "wqe",
        "weq",
    };

    private SequenceGenerator generator;
    private Random random = new Random();

    @Inject
    public BalanceRequestFactoryImpl(SequenceGenerator generator) {
        this.generator = generator;
    }

    @Override
    public BalanceRequest getRequest() {
        long id = generator.generate();
        int rndInt = Math.abs(random.nextInt());
        String name = (rndInt % 10 == 0) ? blackListedNames[rndInt % blackListedNames.length] : String.format("USER%02d", id % 100);
        String transactionId = generator.generate("TX%08X");

        BigDecimal balanceChange = BigDecimal.valueOf((random.nextDouble() - 0.5) * 2000).setScale(2, RoundingMode.HALF_UP);

        return new BalanceRequest(name, transactionId, balanceChange);
    }
}
