package com.playtech.balance.client;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.playtech.util.config.InputStreamProvider;
import com.playtech.util.config.impl.SimpleInputStreamProvider;

public class Main {
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    @Inject
    @Named("client.requests")
    private int numRequests;

    @Inject
    private ExecutorService executor;

    @Inject
    private Injector injector;


    public static void main(String[] args) throws InterruptedException {
        InputStreamProvider provider = new SimpleInputStreamProvider(".");

        String configName = "client.config";

        Injector injector = Guice.createInjector(new ClientModule(provider, configName));
        Main main = injector.getInstance(Main.class);

        main.run();
    }

    public void run() throws InterruptedException {
        log.info("Submitting jobs");
        for(int i = 0; i < numRequests; i++) {
            executor.submit(injector.getInstance(Client.class));
        }
        log.info("All jobs submitted");
        executor.shutdown();
        executor.awaitTermination(10, TimeUnit.MINUTES);
        log.info("Client done");
    }
}
