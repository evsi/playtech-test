package com.playtech.balance.client;

import com.google.inject.ImplementedBy;
import com.playtech.balance.client.impl.ClientImpl;

@ImplementedBy(ClientImpl.class)
public interface Client extends Runnable {
}
