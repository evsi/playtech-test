package com.playtech.balance.client;

import java.time.Clock;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.playtech.util.Utils;
import com.playtech.util.config.Configuration;
import com.playtech.util.config.InputStreamProvider;
import com.playtech.util.config.impl.ConfigurationLoaderImpl;
import com.playtech.util.sequence.impl.SequenceGeneratorImpl;

public class ClientModule extends AbstractModule {
    private static final String CLIENT_NUM_THREADS = "client.threads";
    private static final String CLIENT_NUM_REQUESTS = "client.requests";
    private static final int DEFAULT_NUM_THREADS = 3;
    private static final int DEFAULT_NUM_REQUESTS = 100;

    private InputStreamProvider provider;
    private String configName;

    public ClientModule(InputStreamProvider provider, String configName) {
        this.provider = provider;
        this.configName = configName;
    }

    @Override
    protected void configure() {
        Configuration config = loadConfig();

        bind(Configuration.class).toInstance(config);
        config.getNames().forEach((name) -> bind(String.class).annotatedWith(Names.named(name)).toInstance(config.get(name)));

        long millis = Clock.systemUTC().millis();
        bind(Long.class).annotatedWith(Names.named(SequenceGeneratorImpl.INITIAL_CLOCK_MILLIS)).toInstance(millis);

        int numThreads  = Utils.parseInt(config.get(CLIENT_NUM_THREADS), DEFAULT_NUM_THREADS);
        int numRequests = Utils.parseInt(config.get(CLIENT_NUM_REQUESTS), DEFAULT_NUM_REQUESTS);

        bind(Integer.class).annotatedWith(Names.named(CLIENT_NUM_THREADS)).toInstance(numThreads);
        bind(Integer.class).annotatedWith(Names.named(CLIENT_NUM_REQUESTS)).toInstance(numRequests);

        bind(ExecutorService.class).toInstance(Executors.newFixedThreadPool(numThreads));
    }

    private Configuration loadConfig() {
        return new ConfigurationLoaderImpl(provider).load(configName, true);
    }
}
