package com.playtech.balance.client.impl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicLong;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.playtech.balance.BalanceRequest;
import com.playtech.balance.BalanceResponse;
import com.playtech.balance.client.BalanceRequestFactory;
import com.playtech.balance.client.Client;
import com.playtech.balance.server.AuthRequest;
import com.playtech.balance.server.AuthResponse;
import com.playtech.util.DigestUtils;
import com.playtech.util.SerialUtils;
import com.playtech.util.Utils;

public class ClientImpl implements Client {
    public static final Logger log = LoggerFactory.getLogger(ClientImpl.class);
    private static final AtomicLong requestCounter = new AtomicLong();

    private static final String SERVER_HOST = "server.host";
    private static final String SERVER_PORT = "server.port";
    private static final String SERVER_LOGIN = "server.login";
    private static final String SERVER_PASSWORD = "server.password";

    private static final int DEFAULT_PORT = 8095;

    private BalanceRequestFactory factory;
    private int port;

    private String login;
    private String password;
    private InetAddress addr;

    @Inject
    public ClientImpl(BalanceRequestFactory factory,
                      @Named(SERVER_HOST) String host,
                      @Named(SERVER_PORT) String portName,
                      @Named(SERVER_LOGIN) String login,
                      @Named(SERVER_PASSWORD) String password) {
        this.factory = factory;
        this.login = login;
        this.password = password;

        port = Utils.parseInt(portName, DEFAULT_PORT);
        try {
            addr = InetAddress.getByName(host);
        } catch (UnknownHostException e) {
            addr = null;
            log.error("Unable to resolve server host name " + host);
        }
    }

    @Override
    public void run() {
        if (addr == null) {
            log.error("Unable to resolve server host name");
        }
        try (Socket socket = new Socket(addr, port);
             ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream ois = new ObjectInputStream(socket.getInputStream())) {

            authenticate(oos, ois);
            generateRequest(oos, ois);
            oos.flush();
        } catch (Throwable e) {
            log.warn("Unable to connect to server", e);
        }
    }

    private void generateRequest(ObjectOutputStream oos, ObjectInputStream ois) throws IOException {
        BalanceRequest request = factory.getRequest();
        log.info("Updating balance: " + request);
        SerialUtils.safeWrite(oos, request);
        BalanceResponse response = SerialUtils.safeRead(ois, BalanceResponse.class);
        log.info("Balance update result (" + requestCounter.incrementAndGet() + "): " + response);

    }

    private void authenticate(ObjectOutputStream oos, ObjectInputStream ois) throws IOException {
        AuthRequest request = new AuthRequest(login);
        SerialUtils.safeWrite(oos, request);

        String salt = SerialUtils.safeRead(ois, AuthResponse.class).getData();
        SerialUtils.safeWrite(oos, new AuthRequest(DigestUtils.sha256(password + salt)));
        log.debug("Successfully authenticated to server");
    }
}
