package com.playtech.balance.client;

import com.google.inject.ImplementedBy;
import com.playtech.balance.BalanceRequest;
import com.playtech.balance.client.impl.BalanceRequestFactoryImpl;

@ImplementedBy(BalanceRequestFactoryImpl.class)
public interface BalanceRequestFactory {

    BalanceRequest getRequest();

}
