package com.playtech.balance;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;

import org.junit.Test;

import com.playtech.util.SerialUtils;


public class BalanceResponseTest {

    @Test
    public void balanceResponseIsSuccessfullyPassedViaWire() throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        BalanceResponse response = new BalanceResponse(1, 12345L, "transactionId", BigDecimal.valueOf(45.67), BigDecimal.valueOf(89.01));
        SerialUtils.safeWrite(oos, response);
        oos.close();

        Object obj = new ObjectInputStream(new ByteArrayInputStream(baos.toByteArray())).readObject();

        assertTrue(obj instanceof BalanceResponse);
        assertTrue(obj != response);
        assertTrue(obj.equals(response));
    }

    @Test(expected=IOException.class)
    public void ifSignatureIsBrokenThenExceptionIsThrown() throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        BalanceResponse response = new BalanceResponse(1, 12345L, "transactionId", BigDecimal.valueOf(45.67), BigDecimal.valueOf(89.01));
        SerialUtils.safeWrite(oos, response);
        oos.close();

        byte[] buf = baos.toByteArray();
        buf[59] = 0;
        new ObjectInputStream(new ByteArrayInputStream(buf)).readObject();
    }


    @Test
    public void responseIsValidIfAllFieldsPresentAndInRequiredRange() throws Exception {
        BalanceResponse response = new BalanceResponse(1, 12345L, "transactionId", BigDecimal.valueOf(45.67), BigDecimal.valueOf(89.01));
        assertTrue(response.isValid());
    }

    @Test
    public void ifErrorCodeIsUnknownResponseIsInvalid() throws Exception {
        BalanceResponse response = new BalanceResponse(-1, 12345L, "transactionId", BigDecimal.valueOf(45.67), BigDecimal.valueOf(89.01));
        assertFalse(response.isValid());
    }

    @Test
    public void ifTransactionIdIsEmptyResponseIsInvalid() throws Exception {
        BalanceResponse response = new BalanceResponse(1, 12345L, "", BigDecimal.valueOf(45.67), BigDecimal.valueOf(89.01));
        assertFalse(response.isValid());
    }

    @Test
    public void ifTransactionIdIsNullResponseIsInvalid() throws Exception {
        BalanceResponse response = new BalanceResponse(1, 12345L, null, BigDecimal.valueOf(45.67), BigDecimal.valueOf(89.01));
        assertFalse(response.isValid());
    }

    @Test
    public void ifBalanceChangeIsNullResponseIsInvalid() throws Exception {
        BalanceResponse response = new BalanceResponse(1, 12345L, "transactionId", null, BigDecimal.valueOf(89.01));
        assertFalse(response.isValid());
    }

    @Test
    public void ifBalanceIsNullResponseIsInvalid() throws Exception {
        BalanceResponse response = new BalanceResponse(1, 12345L, "transactionId", BigDecimal.valueOf(45.67), null);
        assertFalse(response.isValid());
    }

    @Test
    public void ifVersionIsZeroResponseIsInvalid() throws Exception {
        BalanceResponse response = new BalanceResponse(1, 0, "transactionId", BigDecimal.valueOf(45.67), BigDecimal.valueOf(89.01));
        assertFalse(response.isValid());
    }

    @Test
    public void ifVersionIsLessThanZeroResponseIsInvalid() throws Exception {
        BalanceResponse response = new BalanceResponse(1, -1, "transactionId", BigDecimal.valueOf(45.67), BigDecimal.valueOf(89.01));
        assertFalse(response.isValid());
    }
}
