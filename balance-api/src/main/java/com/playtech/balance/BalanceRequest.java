package com.playtech.balance;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.math.BigDecimal;

import com.playtech.util.Utils;

public class BalanceRequest implements Externalizable {
    private String userName;
    private String transactionId;
    private BigDecimal balanceChange;

    public BalanceRequest() {
    }

    public BalanceRequest(String userName, String transactionId, BigDecimal balanceChange) {
        this.userName = userName;
        this.transactionId = transactionId;
        this.balanceChange = balanceChange;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public BigDecimal getBalanceChange() {
        return balanceChange;
    }

    public void setBalanceChange(BigDecimal balanceChange) {
        this.balanceChange = balanceChange;
    }

    public boolean isValid() {
        return !Utils.isEmpty(userName) && !Utils.isEmpty(transactionId) && balanceChange != null;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(Signature.BALANCE_REQUEST_SIGNATURE);
        out.writeObject(userName);
        out.writeObject(transactionId);
        out.writeObject(balanceChange);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        if (in.readInt() != Signature.BALANCE_REQUEST_SIGNATURE) {
            throw new IOException("Invalid request signature");
        }

        userName = (String) in.readObject();
        transactionId = (String) in.readObject();
        balanceChange = (BigDecimal) in.readObject();
     }

    @Override
    public String toString() {
        return "request [userName=" + userName + ", transactionId=" + transactionId + ", balanceChange="
                + balanceChange + "]";
    }
}
