package com.playtech.balance;

public interface Signature {
    public static final int BALANCE_REQUEST_SIGNATURE = 0xDEADBEEF;
    public static final int BALANCE_RESPONSE_SIGNATURE = 0xCAFEBABE;
}
