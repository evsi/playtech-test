package com.playtech.balance;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.math.BigDecimal;

import com.playtech.util.Utils;

public class BalanceResponse implements Externalizable {
    private int errorCode;
    private long version;
    private String transactionId;
    private BigDecimal balanceChange;
    private BigDecimal balance;

    public BalanceResponse() {
    }

    public BalanceResponse(int errorCode, String transactionId) {
        this.errorCode = errorCode;
        this.version = 1L;
        this.transactionId = transactionId;
        this.balanceChange = BigDecimal.valueOf(0);
        this.balance = BigDecimal.valueOf(0);
    }

    public BalanceResponse(int errorCode,
                           long version,
                           String transactionId,
                           BigDecimal balanceChange,
                           BigDecimal balance) {
        this.errorCode = errorCode;
        this.version = version;
        this.transactionId = transactionId;
        this.balanceChange = balanceChange;
        this.balance = balance;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public BigDecimal getBalanceChange() {
        return balanceChange;
    }

    public void setBalanceChange(BigDecimal change) {
        this.balanceChange = change;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public boolean isValid() {
        return !Utils.isEmpty(transactionId) && balanceChange != null && balance != null && version > 0
                && ErrorCodes.isValid(errorCode);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((balance == null) ? 0 : balance.hashCode());
        result = prime * result + ((balanceChange == null) ? 0 : balanceChange.hashCode());
        result = prime * result + errorCode;
        result = prime * result + ((transactionId == null) ? 0 : transactionId.hashCode());
        result = prime * result + (int) (version ^ (version >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof BalanceResponse)) {
            return false;
        }
        BalanceResponse other = (BalanceResponse) obj;
        if (balance == null) {
            if (other.balance != null) {
                return false;
            }
        } else if (!balance.equals(other.balance)) {
            return false;
        }
        if (balanceChange == null) {
            if (other.balanceChange != null) {
                return false;
            }
        } else if (!balanceChange.equals(other.balanceChange)) {
            return false;
        }
        if (errorCode != other.errorCode) {
            return false;
        }
        if (transactionId == null) {
            if (other.transactionId != null) {
                return false;
            }
        } else if (!transactionId.equals(other.transactionId)) {
            return false;
        }
        if (version != other.version) {
            return false;
        }
        return true;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(Signature.BALANCE_RESPONSE_SIGNATURE);
        out.writeInt(errorCode);
        out.writeLong(version);
        out.writeObject(transactionId);
        out.writeObject(balanceChange);
        out.writeObject(balance);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

        if (in.readInt() != Signature.BALANCE_RESPONSE_SIGNATURE) {
            throw new IOException("Invalid response signature");
        }

        errorCode = in.readInt();
        version = in.readLong();
        transactionId = (String) in.readObject();
        balanceChange = (BigDecimal) in.readObject();
        balance = (BigDecimal) in.readObject();
    }

    @Override
    public String toString() {
        return "response [errorCode=" + errorCode + "(" + ErrorCodes.getName(errorCode) + ")" + ", version=" + version + ", transactionId=" + transactionId
                + ", balanceChange=" + balanceChange + ", balance=" + balance + "]";
    }
}
