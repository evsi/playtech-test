package com.playtech.balance.server;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class AuthResponse implements Externalizable {
    private String data;

    public AuthResponse() {
    }

    public AuthResponse(String data) {
        this.data = data;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(Signature.AUTH_RESPONSE);
        out.writeObject(data);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        if (in.readInt() != Signature.AUTH_RESPONSE) {
            throw new IOException("Invalid response signature");
        }

        data = (String) in.readObject();
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
