package com.playtech.balance.server;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class AuthRequest implements Externalizable {
    private String data;

    public AuthRequest() {
    }

    public AuthRequest(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(Signature.AUTH_REQUEST);
        out.writeObject(data);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        if (in.readInt() != Signature.AUTH_REQUEST) {
            throw new IOException("Invalid request signature");
        }

        data = (String) in.readObject();
    }
}
