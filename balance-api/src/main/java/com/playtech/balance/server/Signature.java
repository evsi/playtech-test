package com.playtech.balance.server;

public interface Signature {
    public static final int AUTH_REQUEST = 0xBEEFDEAD;
    public static final int AUTH_RESPONSE = 0xBABECAFE;
}
