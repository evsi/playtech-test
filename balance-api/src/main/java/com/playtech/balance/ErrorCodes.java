package com.playtech.balance;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class ErrorCodes {
    public static final int OK = 0;
    public static final int PLAYER_BLACKLISTED = 1;
    public static final int CHANGE_EXCEEDS_LIMIT = 2;
    public static final int CHANGE_EXCEEDS_BALANCE = 3;
    public static final int SERVER_ERROR = 101;

    private static final Set<Integer> validCodes = new HashSet<>(Arrays.asList(OK, PLAYER_BLACKLISTED, CHANGE_EXCEEDS_LIMIT, CHANGE_EXCEEDS_BALANCE, SERVER_ERROR));
    private static final Map<Integer, String> errorNames = new HashMap<>();

    static {
        errorNames.put(OK, "Success");
        errorNames.put(PLAYER_BLACKLISTED, "Player is blacklisted");
        errorNames.put(CHANGE_EXCEEDS_LIMIT, "Change exceeds configured limit");
        errorNames.put(CHANGE_EXCEEDS_BALANCE, "Change exceeds balance");
        errorNames.put(SERVER_ERROR, "Error at server side");
    }

    private ErrorCodes() {
    }

    public static String getName(int errorCode) {
        return errorNames.get(errorCode);
    }

    public static boolean isValid(int errorCode) {
        return validCodes.contains(errorCode);
    }
}
