package com.playtech.balance.server;

import com.google.inject.ImplementedBy;
import com.playtech.balance.server.impl.ProcessRegistryImpl;

@ImplementedBy(ProcessRegistryImpl.class)
public interface ProcessRegistry {
    void register(ActiveProcess process);
    void deregister(ActiveProcess process);

    void stop();

    int count();
}
