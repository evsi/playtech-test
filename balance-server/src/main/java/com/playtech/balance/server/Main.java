package com.playtech.balance.server;

import java.io.IOException;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.persist.PersistService;
import com.playtech.util.config.InputStreamProvider;
import com.playtech.util.config.impl.SimpleInputStreamProvider;

public class Main {

    public static void main(String[] args) {
        System.out.println("Starting server.");

        InputStreamProvider provider = new SimpleInputStreamProvider(".");
        String configName = "server.config";

        Injector injector = Guice.createInjector(new ServerModule(provider, configName));
        PersistService persistService = injector.getInstance(PersistService.class);
        persistService.start();

        try {
            Server server = injector.getInstance(Server.class);
            server.start();
            waitForKeyPress();
            stopServer(server);
        } finally {
            persistService.stop();
        }
    }

    private static void stopServer(Server server) {
        System.out.print("Stopping server");
        server.stop();
        while (!server.isStopped()) {
            System.out.print(".");
            sleep(1);
        }

        System.out.println();
        System.out.println("Server is stopped.");
    }

    private static void waitForKeyPress() {
        System.out.println("Server started. Press any key to stop it...");
        try {
            System.in.read();
        } catch (IOException e) {
            // intentionally ignore
        }
    }

    private static void sleep(int i) {
        try {
            Thread.sleep(i * 1000);
        } catch (InterruptedException e) {
            // intentionally ignore
        }
    }
}
