package com.playtech.balance.server;

public interface ActiveProcess {
    ActiveProcess start();

    ActiveProcess stop();

    String getId();
}
