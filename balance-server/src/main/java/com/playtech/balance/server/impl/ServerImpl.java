package com.playtech.balance.server.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.inject.Injector;
import com.playtech.balance.server.ProcessRegistry;
import com.playtech.balance.server.Server;
import com.playtech.balance.server.ServerListener;
import com.playtech.balance.service.ReplicationProcessor;

@Singleton
public class ServerImpl implements Server {
    private static final String SERVER_ID = "SERVER_ID";

    private Injector injector;
    private ProcessRegistry processRegistry;
    private ReplicationProcessor replicationProcessor;
    private ExecutorService replicationExecutor;

    @Inject
    public ServerImpl(Injector injector, ProcessRegistry processRegistry, ReplicationProcessor replicationProcessor) {
        this.injector = injector;
        this.processRegistry = processRegistry;
        this.replicationProcessor = replicationProcessor;
        replicationExecutor = Executors.newSingleThreadExecutor();
    }

    @Override
    public synchronized Server start() {
        if (!isStopped()) {
            throw new RuntimeException("Attempt to start already running server");
        }

        try {
            injector.getInstance(ServerListener.class).start();
            replicationExecutor.submit(() -> replicationProcessor.replicate());
            return this;
        } catch (Exception e) {
            throw new RuntimeException("Unable to start server", e);
        }
    }

    @Override
    public synchronized Server stop() {
        processRegistry.stop();
        replicationProcessor.stop();
        replicationExecutor.shutdown();
        try {
            replicationExecutor.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            //Intentionally ignore
        }
        return this;
    }

    @Override
    public boolean isStopped() {
        return processRegistry.count() == 0;
    }

    @Override
    public String getId() {
        return SERVER_ID;
    }
}
