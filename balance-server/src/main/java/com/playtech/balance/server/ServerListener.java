package com.playtech.balance.server;

import com.google.inject.ImplementedBy;
import com.playtech.balance.server.impl.ServerListenerImpl;

@ImplementedBy(ServerListenerImpl.class)
public interface ServerListener extends ActiveProcess {
}
