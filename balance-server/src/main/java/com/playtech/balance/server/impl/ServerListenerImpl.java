package com.playtech.balance.server.impl;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Injector;
import com.playtech.balance.server.ProcessRegistry;
import com.playtech.balance.server.RequestHandler;
import com.playtech.balance.server.ServerListener;
import com.playtech.util.Utils;

@Singleton
public class ServerListenerImpl implements ServerListener {
    private static final Logger log = LoggerFactory.getLogger(ServerListenerImpl.class);

    private static final String SERVER_LISTENER_ID = "serverListener";

    private static final int DEFAULT_PORT = 8095;
    private static final int DEFAULT_BACKLOG = 50;

    @Inject
    private Injector injector;

    @Inject
    private ProcessRegistry processRegistry;

    @Inject
    @Named("server.port")
    private String portName;

    @Inject
    @Named("server.backlog")
    private String backlogName;

    @Inject
    @Named("server.thread.pool")
    private ExecutorService executorService;

    private ServerSocket serverSocket;

    private volatile boolean running = true;

    @Override
    public ServerListener stop() {
        running = false;
        Utils.close(serverSocket);
        processRegistry.deregister(this);
        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            // intentionally ignore
        }
        return this;
    }

    @Override
    public ServerListener start() {
        int port = Utils.parseInt(portName, DEFAULT_PORT);
        int backLog = Utils.parseInt(backlogName, DEFAULT_BACKLOG);

        try {
            serverSocket = new ServerSocket(port, backLog);
            log.info("Starting listener at " + port + ", backlog " + backLog);

            executorService.execute(() -> {processRegistry.register(this); serve();});
        } catch (IOException e) {
            throw new RuntimeException("Unable to start server", e);
        }
        return this;
    }

    private void serve() {
        log.info("Listening for incoming connections");
        while (running) {
            serveIncomingConnections();
        }
    }

    private void serveIncomingConnections() {
        try {
            Socket socket = serverSocket.accept();
            executorService.execute(() -> {injector.getInstance(RequestHandler.class).handle(socket).start(); });
        } catch (SocketException se) {
            log.warn("Socket exception: " + se.getMessage());
        } catch (IOException e) {
            log.error("Server socket I/O error", e);
        }
    }

    @Override
    public String getId() {
        return SERVER_LISTENER_ID;
    }
}
