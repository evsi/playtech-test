package com.playtech.balance.server.impl;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.inject.Singleton;

import com.playtech.balance.server.ActiveProcess;
import com.playtech.balance.server.ProcessRegistry;

@Singleton
public class ProcessRegistryImpl implements ProcessRegistry {
    private ConcurrentMap<String, ActiveProcess> processes = new ConcurrentHashMap<>();

    @Override
    public void register(ActiveProcess process) {
        ActiveProcess existing = processes.putIfAbsent(process.getId(), process);

        if (existing != null) {
            throw new RuntimeException("Attempt to register process with same ID " + process.getId());
        }
    }

    @Override
    public void deregister(ActiveProcess process) {
        if (process != null && process.getId() != null) {
            processes.remove(process.getId());
        }
    }

    @Override
    public void stop() {
        processes.values().forEach(process -> process.stop());
    }

    @Override
    public int count() {
        return processes.size();
    }
}
