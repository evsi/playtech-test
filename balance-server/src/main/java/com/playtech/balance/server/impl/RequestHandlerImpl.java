package com.playtech.balance.server.impl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.playtech.balance.BalanceRequest;
import com.playtech.balance.server.ActiveProcess;
import com.playtech.balance.server.AuthRequest;
import com.playtech.balance.server.AuthResponse;
import com.playtech.balance.server.ProcessRegistry;
import com.playtech.balance.server.RequestHandler;
import com.playtech.balance.service.BalanceService;
import com.playtech.util.DigestUtils;
import com.playtech.util.SerialUtils;
import com.playtech.util.Utils;
import com.playtech.util.sequence.SequenceGenerator;

public class RequestHandlerImpl implements RequestHandler {
    private static final Logger log = LoggerFactory.getLogger(RequestHandlerImpl.class);

    private BalanceService service;
    private ProcessRegistry processRegistry;
    private SequenceGenerator generator;
    private String login;
    private String password;

    private String id;
    private Socket socket;

    @Inject
    public RequestHandlerImpl(BalanceService service,
                              ProcessRegistry processRegistry,
                              SequenceGenerator generator,
                              @Named("server.login") String login,
                              @Named("server.password") String password) {
        this.service = service;
        this.processRegistry = processRegistry;
        this.generator = generator;
        this.login = login;
        this.password = password;
    }

    @Override
    public RequestHandler start() {
        if (socket == null) {
            log.warn("Attempt to start handler without socket.");
            return this;
        }
        try {
            id = generator.generate("RH%08X");
            processRegistry.register(this);
            log.debug("Handling request from " + socket.getRemoteSocketAddress() + " started by " + id);
            handleRequest();
        } catch (Throwable t) {
            log.warn("Exception during request handling", t);
        } finally {
            log.debug("Handling request from " + ((socket == null) ? "(unknown)" : socket.getRemoteSocketAddress()) + " done.");
            stop();
        }
        return this;
    }

    private void handleRequest() {
        try (ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
             ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream())) {
            authenticate(ois, oos);
            BalanceRequest req = SerialUtils.safeRead(ois, BalanceRequest.class);
            if (req.isValid()) {
                SerialUtils.safeWrite(oos, service.serve(req));
            }
        } catch (IOException e) {
            log.warn("Error during request handling", e);
        }
    }

    private void authenticate(ObjectInputStream ois, ObjectOutputStream oos) throws IOException {
        AuthRequest request = SerialUtils.safeRead(ois, AuthRequest.class);

        if (!login.equals(request.getData())) {
            throw new IOException("Invalid login " + request.getData());
        }

        String salt = DigestUtils.generateUID("AUTH");
        String expectedDigest = DigestUtils.sha256(password + salt);

        SerialUtils.safeWrite(oos, new AuthResponse(salt));
        request = SerialUtils.safeRead(ois, AuthRequest.class);

        if (!expectedDigest.equals(request.getData())) {
            throw new IOException("Invalid password");
        }
    }

    @Override
    public ActiveProcess stop() {
        Utils.close(socket);
        socket = null;
        processRegistry.deregister(this);
        return this;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public RequestHandler handle(Socket socket) {
        this.socket = socket;
        return this;
    }
}
