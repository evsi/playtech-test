package com.playtech.balance.server;

import com.google.inject.ImplementedBy;
import com.playtech.balance.server.impl.ServerImpl;

@ImplementedBy(ServerImpl.class)
public interface Server extends ActiveProcess {
    boolean isStopped();
}
