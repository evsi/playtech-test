package com.playtech.balance.server;

import java.net.Socket;

import com.google.inject.ImplementedBy;
import com.playtech.balance.server.impl.RequestHandlerImpl;

@ImplementedBy(RequestHandlerImpl.class)
public interface RequestHandler extends ActiveProcess {

    RequestHandler handle(Socket socket);
}
