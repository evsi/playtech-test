package com.playtech.balance.server;

import java.time.Clock;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.playtech.balance.server.annotation.Step1;
import com.playtech.balance.server.annotation.Step2;
import com.playtech.balance.server.annotation.Step3;
import com.playtech.balance.service.BalanceDao;
import com.playtech.balance.service.BalanceService;
import com.playtech.balance.service.impl.BalanceServiceImpl;
import com.playtech.balance.service.impl.CachingBalanceDao;
import com.playtech.balance.service.impl.LoggingBalanceService;
import com.playtech.balance.service.impl.PersistentBalanceDao;
import com.playtech.balance.service.impl.TransactionCachingBalanceService;
import com.playtech.balance.service.impl.ValidatingBalanceService;
import com.playtech.util.config.Configuration;
import com.playtech.util.config.InputStreamProvider;
import com.playtech.util.config.impl.ConfigurationLoaderImpl;
import com.playtech.util.sequence.impl.SequenceGeneratorImpl;

public class ServerModule extends AbstractModule {
    private static final Logger log = LoggerFactory.getLogger(ServerModule.class);

    private static final String JPA_UNIT = "jpaUnit";
    private static final String SERVER_THREAD_POOL = "server.thread.pool";
    private static final String DEFAULT_THREAD_POOL_SIZE_TEXT = "10";
    private static final int DEFAULT_THREAD_POOL_SIZE = 10;

    private InputStreamProvider provider;
    private String configName;

    public ServerModule(InputStreamProvider provider, String configName) {
        this.provider = provider;
        this.configName = configName;
    }

    @Override
    protected void configure() {
        install(new JpaPersistModule(JPA_UNIT));

        Configuration config = loadConfig();

        bind(Configuration.class).toInstance(config);
        config.getNames().forEach((name) -> bind(String.class).annotatedWith(Names.named(name)).toInstance(config.get(name)));

        int size = determineThreadPoolSize(config);
        bind(ExecutorService.class).annotatedWith(Names.named(SERVER_THREAD_POOL)).toInstance(Executors.newFixedThreadPool(size));

        long millis = Clock.systemUTC().millis();
        bind(Long.class).annotatedWith(Names.named(SequenceGeneratorImpl.INITIAL_CLOCK_MILLIS)).toInstance(millis);

        bind(BalanceService.class).to(LoggingBalanceService.class);
        bind(BalanceService.class).annotatedWith(Step1.class).to(ValidatingBalanceService.class);
        bind(BalanceService.class).annotatedWith(Step2.class).to(TransactionCachingBalanceService.class);
        bind(BalanceService.class).annotatedWith(Step3.class).to(BalanceServiceImpl.class);

        bind(BalanceDao.class).to(CachingBalanceDao.class);
        bind(BalanceDao.class).annotatedWith(Step3.class).to(PersistentBalanceDao.class);
    }

    private int determineThreadPoolSize(Configuration config) {
        String sizeText = config.get(SERVER_THREAD_POOL, DEFAULT_THREAD_POOL_SIZE_TEXT);

        try {
            return Integer.parseInt(sizeText);
        } catch (NumberFormatException e) {
            log.warn("Unable to parse configured thread pool size: " + sizeText + ". Using default " + DEFAULT_THREAD_POOL_SIZE_TEXT);
            return DEFAULT_THREAD_POOL_SIZE;
        }
    }

    private Configuration loadConfig() {
        return new ConfigurationLoaderImpl(provider).load(configName, true);
    }
}
