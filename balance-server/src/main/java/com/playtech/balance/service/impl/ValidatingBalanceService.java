package com.playtech.balance.service.impl;

import java.math.BigDecimal;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.playtech.balance.BalanceRequest;
import com.playtech.balance.BalanceResponse;
import com.playtech.balance.ErrorCodes;
import com.playtech.balance.server.annotation.Step2;
import com.playtech.balance.service.BalanceService;
import com.playtech.balance.service.BlacklistingService;

public class ValidatingBalanceService implements BalanceService {
    private static final Logger log = LoggerFactory.getLogger(ValidatingBalanceService.class);

    private static final BigDecimal DEFAULT_BALANCE_CHANGE_LIMIT = BigDecimal.valueOf(1000L);

    private BalanceService delegate;
    private BigDecimal changeLimit;
    private BlacklistingService blacklistingService;

    @Inject
    public ValidatingBalanceService(@Step2 BalanceService delegate, BlacklistingService blacklistingService, @Named("balance.change.limit") String changeLimitName) {
        this.delegate = delegate;
        this.blacklistingService = blacklistingService;

        try {
            changeLimit = BigDecimal.valueOf(Double.parseDouble(changeLimitName));
        } catch (NumberFormatException e) {
            log.info("Incorrectly configured change limit " + changeLimitName + ", using default " + DEFAULT_BALANCE_CHANGE_LIMIT);
        }
    }

    @Override
    public BalanceResponse serve(BalanceRequest req) {
        if (isBlacklisted(req.getUserName())) {
            return new BalanceResponse(ErrorCodes.PLAYER_BLACKLISTED, req.getTransactionId());
        }

        if (req.getBalanceChange().abs().compareTo(changeLimit) > 0) {
            return new BalanceResponse(ErrorCodes.CHANGE_EXCEEDS_LIMIT, req.getTransactionId());
        }

        return delegate.serve(req);
    }

    private boolean isBlacklisted(String userName) {
        return blacklistingService.isBlacklisted(userName);
    }
}
