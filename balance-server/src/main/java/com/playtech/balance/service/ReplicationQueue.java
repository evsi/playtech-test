package com.playtech.balance.service;

import com.google.inject.ImplementedBy;
import com.playtech.balance.entity.BalanceEntity;
import com.playtech.balance.service.impl.ReplicationQueueImpl;

@ImplementedBy(ReplicationQueueImpl.class)
public interface ReplicationQueue {

    void put(BalanceEntity response);

    BalanceEntity retrieve();

    void stop();
}
