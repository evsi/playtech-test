package com.playtech.balance.service.impl;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.playtech.balance.entity.BalanceEntity;
import com.playtech.balance.service.ReplicationQueue;

@Singleton
public class ReplicationQueueImpl implements ReplicationQueue {
    private static final Logger log = LoggerFactory.getLogger(ReplicationQueueImpl.class);

    private BlockingQueue<BalanceEntity> queue = new LinkedBlockingQueue<BalanceEntity>();

    @Override
    public void put(BalanceEntity response) {
        if(!queue.offer(response)) {
            log.warn("Response is not submitted into replication queue");
        }
        log.info("Replication queue size: " + queue.size());
    }

    @Override
    public BalanceEntity retrieve() {
        try {
            return queue.poll(50, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            return null;
        }
    }

    @Override
    public void stop() {
        queue.clear();
        queue.add(new BalanceEntity());
    }
}
