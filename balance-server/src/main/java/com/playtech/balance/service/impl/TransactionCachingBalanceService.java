package com.playtech.balance.service.impl;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.playtech.balance.BalanceRequest;
import com.playtech.balance.BalanceResponse;
import com.playtech.balance.ErrorCodes;
import com.playtech.balance.server.annotation.Step3;
import com.playtech.balance.service.BalanceService;
import com.playtech.balance.service.TransactionHistory;

@Singleton
public class TransactionCachingBalanceService implements BalanceService {
    private TransactionHistory transactionHistory;
    private BalanceService delegate;

    @Inject
    public TransactionCachingBalanceService(@Step3 BalanceService delegate, TransactionHistory transactionHistory) {
        this.delegate = delegate;
        this.transactionHistory = transactionHistory;
    }

    @Override
    public BalanceResponse serve(BalanceRequest req) {
        BalanceResponse response = transactionHistory.get(req);

        if (response != null) {
            if (response.getErrorCode() == ErrorCodes.OK) {
                transactionHistory.put(response);
            }
            return response;
        }

        return delegate.serve(req);
    }
}
