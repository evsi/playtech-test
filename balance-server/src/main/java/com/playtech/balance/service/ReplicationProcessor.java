package com.playtech.balance.service;

import com.google.inject.ImplementedBy;
import com.playtech.balance.service.impl.ReplicationProcessorImpl;

@ImplementedBy(ReplicationProcessorImpl.class)
public interface ReplicationProcessor {
    void replicate();

    void stop();
}
