package com.playtech.balance.service.impl;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.playtech.balance.BalanceRequest;
import com.playtech.balance.BalanceResponse;
import com.playtech.balance.server.annotation.Step1;
import com.playtech.balance.service.BalanceService;

@Singleton
public class LoggingBalanceService implements BalanceService {
    private static final Logger log = LoggerFactory.getLogger(BalanceService.class);
    private BalanceService delegate;

    @Inject
    public LoggingBalanceService(@Step1 BalanceService delegate) {
        this.delegate = delegate;
    }

    @Override
    public BalanceResponse serve(BalanceRequest req) {
        log.info("Start handling "+ req);

        BalanceResponse response = null;
        try {
            return response = delegate.serve(req);
        } finally {
            log.info("End handling " + req + " " + (response == null ? "no response": response));
        }
    }
}
