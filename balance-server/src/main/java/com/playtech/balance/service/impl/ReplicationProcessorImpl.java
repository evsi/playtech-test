package com.playtech.balance.service.impl;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.playtech.balance.entity.BalanceEntity;
import com.playtech.balance.server.annotation.Step3;
import com.playtech.balance.service.BalanceDao;
import com.playtech.balance.service.ReplicationProcessor;
import com.playtech.balance.service.ReplicationQueue;

@Singleton
public class ReplicationProcessorImpl implements ReplicationProcessor {
    private static final Logger log = LoggerFactory.getLogger(ReplicationProcessorImpl.class);

    private ReplicationQueue queue;
    private BalanceDao balanceDao;

    @Inject
    public ReplicationProcessorImpl(ReplicationQueue queue, @Step3 BalanceDao balanceDao) {
        this.queue = queue;
        this.balanceDao = balanceDao;
    }

    @Override
    public void replicate() {
        log.info("Replication process is started.");
        try {
            doReplicate();
        } finally {
            log.info("Replication process is stopped.");
        }
    }

    private void doReplicate() {
        for (;;) {
            BalanceEntity entity = queue.retrieve();

            if (entity == null) {
                continue;
            }
            
            if (!entity.isValid()) {
                return;
            }

            balanceDao.persist(entity);
            log.info("Persisted " + entity);
        }
    }

    @Override
    public void stop() {
        queue.stop();
    }
}
