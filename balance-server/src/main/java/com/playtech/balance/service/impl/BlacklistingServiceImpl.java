package com.playtech.balance.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.playtech.balance.service.BlacklistingService;
import com.playtech.util.config.Configuration;

@Singleton
public class BlacklistingServiceImpl implements BlacklistingService {
    private static final String BLACKLIST_PROPERTY_NAME = "blacklist.user";
    private Set<String> blackList = new HashSet<>();

    @Inject
    public BlacklistingServiceImpl(Configuration configuration) {
        List<String> users = configuration.getAll(BLACKLIST_PROPERTY_NAME);
        if (users != null) {
            blackList.addAll(users);
        }
    }

    @Override
    public boolean isBlacklisted(String userName) {
        return blackList.contains(userName);
    }
}
