package com.playtech.balance.service.impl;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.playtech.balance.BalanceRequest;
import com.playtech.balance.BalanceResponse;
import com.playtech.balance.entity.BalanceEntity;
import com.playtech.balance.server.annotation.Step3;
import com.playtech.balance.service.BalanceDao;
import com.playtech.balance.service.ReplicationQueue;

@Singleton
public class CachingBalanceDao implements BalanceDao {
    private ConcurrentMap<String, BalanceEntity> cache = new ConcurrentHashMap<>();
    private BalanceDao delegate;
    private ReplicationQueue replicationQueue;

    @Inject
    public CachingBalanceDao(@Step3 BalanceDao delegate, ReplicationQueue replicationQueue) {
        this.delegate = delegate;
        this.replicationQueue = replicationQueue;
    }

    @Override
    public BalanceResponse update(BalanceRequest req) {
        BalanceEntity value = find(req.getUserName());

        synchronized (value) {
            return BalanceEntityUtil.updateEntity(value, req, (e) -> replicationQueue.put(new BalanceEntity(e)));
        }
    }

    private BalanceEntity loadEntity(String userName) {
        BalanceEntity entity = delegate.find(userName);

        if (entity == null) {
            entity = new BalanceEntity(userName, 1, BigDecimal.ZERO);
        }

        return entity;
    }

    @Override
    public BalanceEntity find(String userName) {
        return cache.computeIfAbsent(userName, (k) -> loadEntity(k));
    }

    @Override
    public void persist(BalanceEntity balanceEntity) {
        throw new UnsupportedOperationException("Attempt to persist into in-memory storage");
    }
}
