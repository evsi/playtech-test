package com.playtech.balance.service.impl;

import java.math.BigDecimal;

import com.playtech.balance.BalanceRequest;
import com.playtech.balance.BalanceResponse;
import com.playtech.balance.ErrorCodes;
import com.playtech.balance.entity.BalanceEntity;

public final class BalanceEntityUtil {
    private BalanceEntityUtil() {
    }

    public static BalanceResponse updateEntity(BalanceEntity value, BalanceRequest req, PostProcessFunction postProcess) {
        if (value.getBalance().add(req.getBalanceChange()).compareTo(BigDecimal.ZERO) < 0) {
            return new BalanceResponse(ErrorCodes.CHANGE_EXCEEDS_BALANCE, req.getTransactionId());
        }
        BigDecimal finalAmount = value.getBalance().add(req.getBalanceChange());
        value.setBalance(finalAmount);

        long version = value.getBalanceVersion() + 1;
        value.setBalanceVersion(version);

        if (postProcess != null) {
            postProcess.handle(new BalanceEntity(value));
        }
        return new BalanceResponse(ErrorCodes.OK, version, req.getTransactionId(), req.getBalanceChange(), finalAmount);

    }

    public interface PostProcessFunction {
        void handle(BalanceEntity entity);
    }
}
