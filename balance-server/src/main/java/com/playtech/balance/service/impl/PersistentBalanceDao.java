package com.playtech.balance.service.impl;

import java.math.BigDecimal;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import com.playtech.balance.BalanceRequest;
import com.playtech.balance.BalanceResponse;
import com.playtech.balance.ErrorCodes;
import com.playtech.balance.entity.BalanceEntity;
import com.playtech.balance.service.BalanceDao;

@Singleton
public class PersistentBalanceDao implements BalanceDao {

    private static final Logger log = LoggerFactory.getLogger(PersistentBalanceDao.class);
    private Provider<EntityManager> emProvider;

    @Inject
    public PersistentBalanceDao(Provider<EntityManager> emProvider) {
        this.emProvider = emProvider;
    }

    @Transactional
    @Override
    public BalanceResponse update(BalanceRequest req) {
        EntityManager em = emProvider.get();

        try {
            BalanceEntity entity = em.find(BalanceEntity.class, req.getUserName());

            if (entity == null) {
                entity = new BalanceEntity(req.getUserName(), 1, BigDecimal.ZERO);
                em.persist(entity);
            }

            BalanceResponse response = BalanceEntityUtil.updateEntity(entity, req, null);
            em.merge(entity);
            em.flush();
            return response;
        } catch (IllegalArgumentException | PersistenceException e) {
            log.warn("Exception is thrown while updating entity for request " + req, e);
            return new BalanceResponse(ErrorCodes.SERVER_ERROR, req.getTransactionId());
        }
    }

    @Transactional
    @Override
    public void persist(BalanceEntity balanceEntity) {
        try {
            emProvider.get().merge(balanceEntity);
            emProvider.get().flush();
        } catch (IllegalArgumentException | PersistenceException e) {
            log.warn("Exception is thrown while persisting " + balanceEntity, e);
        }
    }

    @Transactional
    @Override
    public BalanceEntity find(String userName) {
        return emProvider.get().find(BalanceEntity.class, userName);
    }
}
