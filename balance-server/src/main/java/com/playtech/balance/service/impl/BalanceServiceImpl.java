package com.playtech.balance.service.impl;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.playtech.balance.BalanceRequest;
import com.playtech.balance.BalanceResponse;
import com.playtech.balance.service.BalanceDao;
import com.playtech.balance.service.BalanceService;

@Singleton
public class BalanceServiceImpl implements BalanceService {
    private BalanceDao balanceDao;

    @Inject
    public BalanceServiceImpl(BalanceDao balanceDao) {
        this.balanceDao = balanceDao;
    }

    @Override
    public BalanceResponse serve(BalanceRequest req) {
        return balanceDao.update(req);
    }
}
