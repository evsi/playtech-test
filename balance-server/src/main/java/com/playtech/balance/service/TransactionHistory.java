package com.playtech.balance.service;

import com.google.inject.ImplementedBy;
import com.playtech.balance.BalanceRequest;
import com.playtech.balance.BalanceResponse;
import com.playtech.balance.service.impl.TransactionHistoryImpl;

@ImplementedBy(TransactionHistoryImpl.class)
public interface TransactionHistory {
    BalanceResponse get(BalanceRequest req);

    void put(BalanceResponse response);
}
