package com.playtech.balance.service.impl;

import java.util.LinkedHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.inject.Singleton;

import com.playtech.balance.BalanceRequest;
import com.playtech.balance.BalanceResponse;
import com.playtech.balance.service.TransactionHistory;

@Singleton
public class TransactionHistoryImpl implements TransactionHistory {
    private static final int DEFAULT_HISTORY_LENGTH = 1000;

    private ReadWriteLock lock = new ReentrantReadWriteLock(true);
    private LinkedHashMap<String, BalanceResponse> storage = new LinkedHashMap<>();

    @Override
    public void put(BalanceResponse response) {
        try {
            lock.writeLock().lock();

            // make sure that updated record will be at the end of the chain
            storage.remove(response.getTransactionId());
            storage.put(response.getTransactionId(), response);

            if (storage.size() > DEFAULT_HISTORY_LENGTH) {
                String key = storage.keySet().iterator().next();
                storage.remove(key);
            }

        } finally {
            lock.writeLock().unlock();
        }
    }

    @Override
    public BalanceResponse get(BalanceRequest req) {
        try {
            lock.readLock().lock();

            return storage.get(req.getTransactionId());
        } finally {
            lock.readLock().unlock();
        }
    }
}
