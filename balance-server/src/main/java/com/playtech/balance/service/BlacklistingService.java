package com.playtech.balance.service;

import com.google.inject.ImplementedBy;
import com.playtech.balance.service.impl.BlacklistingServiceImpl;

@ImplementedBy(BlacklistingServiceImpl.class)
public interface BlacklistingService {

    boolean isBlacklisted(String userName);

}
