package com.playtech.balance.service;

import com.playtech.balance.BalanceRequest;
import com.playtech.balance.BalanceResponse;
import com.playtech.balance.entity.BalanceEntity;

public interface BalanceDao {
    BalanceResponse update(BalanceRequest req);

    BalanceEntity find(String userName);

    void persist(BalanceEntity balanceEntity);
}
