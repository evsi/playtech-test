package com.playtech.balance.service;

import com.playtech.balance.BalanceRequest;
import com.playtech.balance.BalanceResponse;

public interface BalanceService {

    BalanceResponse serve(BalanceRequest req);
}
