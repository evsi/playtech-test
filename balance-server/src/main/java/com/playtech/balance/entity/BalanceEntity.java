package com.playtech.balance.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.playtech.util.Utils;

/**
 * Entity implementation class for Entity: BalanceEntity
 *
 */
@Entity
@Table(name="PLAYER")
public class BalanceEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private String userName;
    private long balanceVersion;
    private BigDecimal balance;

    public BalanceEntity() {
    }

    public BalanceEntity(String userName, long balanceVersion, BigDecimal balance) {
        this.userName = userName;
        this.balanceVersion = balanceVersion;
        this.balance = balance;
    }

    public BalanceEntity(BalanceEntity value) {
        this(value.getUserName(), value.getBalanceVersion(), value.getBalance());
    }

    @Transient
    public boolean isValid() {
        return !Utils.isEmpty(userName) && balanceVersion > 0 && balance != null;
    }

    @Id
    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name="balance_version")
    public long getBalanceVersion() {
        return this.balanceVersion;
    }

    public void setBalanceVersion(long balanceVersion) {
        this.balanceVersion = balanceVersion;
    }

    public BigDecimal getBalance() {
        return this.balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((balance == null) ? 0 : balance.hashCode());
        result = prime * result + (int) (balanceVersion ^ (balanceVersion >>> 32));
        result = prime * result + ((userName == null) ? 0 : userName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BalanceEntity)) {
            return false;
        }
        BalanceEntity other = (BalanceEntity) obj;
        if (balance == null) {
            if (other.balance != null) {
                return false;
            }
        } else if (!balance.equals(other.balance)) {
            return false;
        }
        if (balanceVersion != other.balanceVersion) {
            return false;
        }
        if (userName == null) {
            if (other.userName != null) {
                return false;
            }
        } else if (!userName.equals(other.userName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[" + userName + ", " + balanceVersion + ", " + balance + "]";
    }
}
