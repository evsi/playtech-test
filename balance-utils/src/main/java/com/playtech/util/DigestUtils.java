package com.playtech.util;

import java.rmi.server.UID;
import java.security.MessageDigest;

public final class DigestUtils {
    private static final char[] DIGITS_LOWER = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    private static final String ZEROS = "00000000000000000000000000000000";

    private DigestUtils() {
    }

    public static String sha256(String value) {
        return digest(value, "SHA-256");
    }

    public static String md5(String value) {
        return digest(value, "MD5");
    }

    private static String digest(String value, String algorithm) {
        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm);
            digest.update(Utils.defaultString(value).getBytes("UTF-8"));
            return new String(encodeHex(digest.digest()));
        } catch (Exception e) {
            return null;
        }
    }

    protected static char[] encodeHex(final byte[] data) {
        final int l = data.length;
        final char[] out = new char[l << 1];

        for (int i = 0, j = 0; i < l; i++) {
            out[j++] = DIGITS_LOWER[(0xF0 & data[i]) >>> 4];
            out[j++] = DIGITS_LOWER[0x0F & data[i]];
        }
        return out;
    }

    public static String generateUID(String prefix) {
        StringBuilder result = new StringBuilder(DigestUtils.ZEROS);
        result.append(new UID().toString().replace(':', '0').replace('-', '0'));
        result.delete(0, result.length() - DigestUtils.ZEROS.length());
        result.insert(0, prefix);
        return result.toString();
    }
}
