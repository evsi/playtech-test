package com.playtech.util;

import java.io.Closeable;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Utils {
    private static final Logger log = LoggerFactory.getLogger(Utils.class);

    private Utils() {
    }

    public static boolean isEmpty(CharSequence cs) {
        return cs == null || cs.length() == 0;
    }

    public static String defaultIfEmpty(String string, String defaultValue) {
        return isEmpty(string) ? defaultValue : string;
    }

    public static String defaultString(String string, String defaultValue) {
        return coalesce(string, defaultValue);
    }

    public static String defaultString(String string) {
        return coalesce(string, "");
    }

    public static <T> boolean isEqual(T string1, T string2) {
        if (string1 == null && string2 == null) {
            return true;
        }
        return string1.equals(string2);
    }

    public static <T> T coalesce(T value1, T value2) {
        return value1 != null ? value1 : value2;
    }

    public static <T> int hashCode(T obj) {
        return obj == null ? 0 : obj.hashCode();
    }

    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                // intentionally ignore
            }
        }
    }

    public static int parseInt(String stringValue, int defaultValue) {
        try {
            return Integer.parseInt(stringValue);
        } catch (NumberFormatException e) {
            log.warn("Unable to parse configuration value " + stringValue + ", using default " + defaultValue);
            return defaultValue;
        }
    }
}
