package com.playtech.util.config;

import java.io.InputStream;

public interface InputStreamProvider {
    InputStream provide(String path);
}
