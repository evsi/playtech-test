package com.playtech.util.config.impl;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.playtech.util.Pair;
import com.playtech.util.Utils;
import com.playtech.util.config.*;

public class ConfigurationLoaderImpl implements ConfigurationLoader {
    private static final Pattern PATTERN = Pattern.compile("(.+)=(.+)");
    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationLoader.class);

    private InputStreamProvider streamProvider;

    @Inject
    public ConfigurationLoaderImpl(InputStreamProvider streamProvider) {
        this.streamProvider = streamProvider;
    }

    @Override
    public Configuration load(String configPath, boolean ignoreEmptySections) {
        try (InputStream stream = streamProvider.provide(configPath)) {
            if (stream != null) {
                return new Configuration(load(stream, ignoreEmptySections));
            }
        } catch (IOException e) {
            LOG.warn("Unable to read configuration from " + configPath, e);
        }
        return null;
    }

    private Map<String, List<String>> load(InputStream stream, boolean ignoreEmptySection) throws IOException {
        Map<String, List<String>> properties = new LinkedHashMap<String, List<String>>();
        BufferedReader br = createBufferedReader(stream);

        String line;
        String section = "";

        while ((line = br.readLine()) != null) {
            line = line.trim();
            if (Utils.isEmpty(line) || line.startsWith("#")) {
                continue;
            }

            if (line.startsWith("[")) {
                section = parseSection(line, section);
                continue;
            }

            if (!ignoreEmptySection && Utils.isEmpty(section)) {
                LOG.info("Declaration outside section, ignoring \"" + line + "\"");
                continue;
            }

            Pair<String, String> variable = parseVariable(line, section);
            if (variable == null) {
                continue;
            }
            getValues(properties, variable.getLeft()).add(variable.getRight());
        }
        return properties;
    }

    protected BufferedReader createBufferedReader(InputStream stream) {
        return new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
    }

    private List<String> getValues(Map<String, List<String>> properties, String key) {
        List<String> result = properties.get(key);
        if (result == null) {
            result = new ArrayList<String>();
            properties.put(key, result);
        }
        return result;
    }

    private Pair<String, String> parseVariable(String line, String section) {
        Matcher matcher = PATTERN.matcher(line);
        if (!matcher.find()) {
            return null;
        }

        return Pair.of((Utils.isEmpty(section) ? "" : section + ".") + matcher.group(1).trim(), matcher.group(2).trim());
    }

    private String parseSection(String line, String section) {
        int idx = line.lastIndexOf(']');
        if (idx < 0) {
            idx = line.length();
        }

        String newSection = line.substring(1, idx).trim();
        return Utils.isEmpty(newSection) ? section : newSection;
    }
}
