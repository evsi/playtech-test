package com.playtech.util.config.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import com.playtech.util.config.InputStreamProvider;

public class SimpleInputStreamProvider implements InputStreamProvider {
    private String basePath;

    public SimpleInputStreamProvider(String basePath) {
        this.basePath = basePath;
    }

    @Override
    public InputStream provide(String path) {
        try {
            return new FileInputStream(new File(basePath, path));
        } catch (FileNotFoundException e) {
            return null;
        }
    }
}
