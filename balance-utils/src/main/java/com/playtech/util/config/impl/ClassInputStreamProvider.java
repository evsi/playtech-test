package com.playtech.util.config.impl;

import java.io.InputStream;

import com.playtech.util.config.InputStreamProvider;

public class ClassInputStreamProvider implements InputStreamProvider {
    private Class<?> clazz;

    public ClassInputStreamProvider(Class<?> clazz) {
        this.clazz = clazz;
    }

    @Override
    public InputStream provide(String path) {
        return clazz.getResourceAsStream(path);
    }
}
