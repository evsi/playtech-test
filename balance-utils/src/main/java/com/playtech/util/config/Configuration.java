package com.playtech.util.config;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.playtech.util.Utils;

public class Configuration {
    private final Map<String, List<String>> properties;

    public Configuration(Map<String, List<String>> properties) {
        this.properties = properties;
    }

    public Set<String> getNames() {
        return properties.keySet();
    }

    public String get(String property) {
        List<String> values = properties.get(property);
        return (values != null && !values.isEmpty()) ? values.get(0) : null;
    }

    public String get(String property, String defaultValue) {
        return Utils.coalesce(get(property), defaultValue);
    }

    public List<String> getAll(String property) {
        return properties.get(property);
    }
}
