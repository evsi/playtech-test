package com.playtech.util.config;

import com.google.inject.ImplementedBy;
import com.playtech.util.config.impl.ConfigurationLoaderImpl;

@ImplementedBy(ConfigurationLoaderImpl.class)
public interface ConfigurationLoader {
    Configuration load(String configName, boolean ignoreEmptySections);
}
