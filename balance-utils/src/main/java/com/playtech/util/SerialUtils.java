package com.playtech.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public final class SerialUtils {
    private SerialUtils() {
    }

    @SuppressWarnings("unchecked")
    public static <T> T safeRead(ObjectInputStream ois, Class<T> clazz) {
        try {
            Object obj = ois.readObject();

            if (clazz.isAssignableFrom(obj.getClass())) {
                return (T) obj;
            }
            throw new RuntimeException("Incoming object of type " + obj.getClass().getName() + " does not match expected " + clazz.getName());
        } catch (ClassNotFoundException | IOException e) {
            throw new RuntimeException("Error while retrieving object from stream", e);
        }
    }

    public static void safeWrite(ObjectOutputStream oos, Object obj) throws IOException {
        oos.writeUnshared(obj);
        oos.flush();
    }
}
