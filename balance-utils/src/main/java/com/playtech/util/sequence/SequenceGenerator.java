package com.playtech.util.sequence;

import com.google.inject.ImplementedBy;
import com.playtech.util.sequence.impl.SequenceGeneratorImpl;

@ImplementedBy(SequenceGeneratorImpl.class)
public interface SequenceGenerator {

    String generate(String prefix);

    long generate();
}
