package com.playtech.util.sequence.impl;

import java.util.concurrent.atomic.AtomicLong;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.playtech.util.sequence.SequenceGenerator;

@Singleton
public class SequenceGeneratorImpl implements SequenceGenerator {
    public static final String INITIAL_CLOCK_MILLIS = "initialClockMillis";

    private AtomicLong sequence;

    @Inject
    public SequenceGeneratorImpl(@Named(INITIAL_CLOCK_MILLIS) long initialValue) {
        sequence = new AtomicLong(initialValue);
    }

    @Override
    public String generate(String format) {
        return String.format(format, sequence.incrementAndGet());
    }

    @Override
    public long generate() {
        return sequence.incrementAndGet();
    }
}
