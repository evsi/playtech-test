package com.playtech.util.config.config.impl;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;

import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.playtech.util.config.Configuration;
import com.playtech.util.config.ConfigurationLoader;
import com.playtech.util.config.impl.ClassInputStreamProvider;
import com.playtech.util.config.impl.ConfigurationLoaderImpl;

@RunWith(EasyMockRunner.class)
public class ConfigurationLoaderImplTest {
    @Mock
    private BufferedReader reader;

    @Test
    public void simpleConfigIsLoadedWithEmptySectionsIgnored() throws Exception {
        Configuration config = load("test1.config", false);

        assertEquals(4, config.getNames().size());
        assertEquals("value1", config.get("section1.var1"));
        assertEquals("value2", config.get("section1.var2"));
        assertEquals("complex value 1", config.get("section1.complex.var1"));
        assertEquals("complex value 2", config.get("section1.complex.var2"));
    }

    @Test
    public void simpleConfigIsLoadedWithEmptySectionsInlcuded() throws Exception {
        Configuration config = load("test1.config", true);

        assertEquals(5, config.getNames().size());
        assertEquals("value1", config.get("section1.var1"));
        assertEquals("value2", config.get("section1.var2"));
        assertEquals("complex value 1", config.get("section1.complex.var1"));
        assertEquals("complex value 2", config.get("section1.complex.var2"));
        assertEquals("some value", config.get("some.var"));
    }

    @Test
    public void ifInputStreamIsNullConfigIsNotLoaded() throws Exception {
        Configuration config = load("non-existent.config", false);
        assertNull(config);
    }
    @Test
    public void multipleValuesAreLoadedProperly() throws Exception {
        Configuration config = load("test2.config", true);

        assertEquals(3, config.getNames().size());

        assertEquals(Arrays.asList("value1", "value2"), config.getAll("section1.var1"));
        assertEquals(Arrays.asList("complex value 1", "complex value 2"), config.getAll("section1.complex.var1"));
        assertEquals(Arrays.asList("some value 1", "some value 2"), config.getAll("some.var"));
    }

    @Test
    public void invalidVariablesValuesAndSectionsAreIgnored() throws Exception {
        Configuration config = load("test3.config", true);
        assertEquals(4, config.getNames().size());
        assertNull(config.get("some.var"));
    }

    @Test
    public void incompleteSectionNameIsHandled() throws Exception {
        Configuration config = load("test4.config", true);
        assertEquals(1, config.getNames().size());
        assertEquals("value 3", config.get("section.var3"));
    }

    @Test
    public void emptyFileIsLoadedSafely() throws Exception {
        Configuration config = load("test5.config", true);
        assertEquals(0, config.getNames().size());
    }

    @Test
    public void IOexceptionsSuppressed() throws Exception {
        ConfigurationLoader loader = new ConfigurationLoaderImpl(new ClassInputStreamProvider(ConfigurationLoaderImplTest.class)) {
            protected BufferedReader createBufferedReader(java.io.InputStream stream) {
                return reader;
            }
        };

        expect(reader.readLine()).andReturn("");
        expect(reader.readLine()).andThrow(new IOException());
        replay(reader);
        assertNull(loader.load("test5.config", true));
    }

    private Configuration load(String configName, boolean ignoreEmptySections) {
        ConfigurationLoader loader = new ConfigurationLoaderImpl(new ClassInputStreamProvider(ConfigurationLoaderImplTest.class));
        return loader.load(configName, ignoreEmptySections);
    }
}

