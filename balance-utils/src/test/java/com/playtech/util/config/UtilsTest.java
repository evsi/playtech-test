package com.playtech.util.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.playtech.util.Utils;

public class UtilsTest {

    @Test
    public void defaultValueIsReturnedIfStringIsNull() throws Exception {
        assertEquals("default", Utils.defaultString(null, "default"));
    }

    @Test
    public void sourceValueIsReturnedIfStringIsNotNull() throws Exception {
        assertEquals("valid", Utils.defaultString("valid", "default"));
    }

    @Test
    public void coalesceWorksForOtherTypes() throws Exception {
        assertEquals(Integer.valueOf(12345), Utils.coalesce(null, new Integer(12345)));
        assertEquals(Integer.valueOf(123456), Utils.coalesce(new Integer(123456), new Integer(12345)));
    }

    @Test
    public void nullObjectsAreEqual() throws Exception {
        assertTrue(Utils.isEqual(null, null));
    }

    @Test
    public void ifNotNullObjectsAreComparedByEquals() throws Exception {
        Object obj1 = new Integer(45678);
        Object obj2 = new Integer(45678);
        assertTrue(obj1.equals(obj2));
        assertTrue(Utils.isEqual(obj1, obj2));
    }

    @Test
    public void nullStringIsEmpty() throws Exception {
        assertTrue(Utils.isEmpty(null));
    }

    @Test
    public void emptyStringIsEmpty() throws Exception {
        assertTrue(Utils.isEmpty(""));
    }

    @Test
    public void notEmptyStringIsNotEmpty() throws Exception {
        assertFalse(Utils.isEmpty(" "));
    }
}
