package com.playtech.util.sequence.impl;

import static org.junit.Assert.*;

import org.junit.Test;

import com.playtech.util.sequence.SequenceGenerator;
import com.playtech.util.sequence.impl.SequenceGeneratorImpl;

//TODO: finish it
public class SequenceGeneratorImplTest {
    @Test
    public void valueIsFormattedCorrecty() throws Exception {
        SequenceGenerator generator = new SequenceGeneratorImpl(0x0102030405060708L);
        assertEquals("TX0102030405060709", generator.generate("TX%016X"));
    }
}
